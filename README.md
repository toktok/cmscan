# cmscan

#### 一· 描述

<p>这是一款全自动化的智能域名采集及指纹识别系统。</p>

#### 二· 系统架构

![image](./docs/设计图.png)

#### 三· 安装

1. 安装Python 3.7.4
2. 执行src/version.py脚本，看看python是否是64bit
3. mysql驱动安装
  - window： 直接安装docs/software/mysql-connector-python**
  - Linux: 执行一下安装命令 pip install mysql-connector-python
  
4. 安装依赖包
```
cd docs/
pip install -r requirement.txt 
```

#### 快速使用

1. 查看帮助
2. 快速使用

<p>启动系统：</p>

```
 # python src/main/cmscan.py
```

<p>扫描特定目标：</p>

```
# python src/main/cmscan.py --target www.easysb.cn dedecms.cn
```

#### 意见及建议

1. 发邮件： 34538980@qq.com
2. 在博客留言：http://www.easysb.cn 

