#
# 建表语句
#
CREATE SCHEMA `cmscan` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ;
create user 'cmscan' identified by 'greatpass';
grant all privileges on cmscan.* to 'cmscan';
flush privileges;
use `cmscan`;

CREATE TABLE IF NOT EXISTS `domain` (
  `id` VARCHAR(34) NOT NULL,
  `domain` VARCHAR(45) NOT NULL,
  `code` SMALLINT NULL DEFAULT 0,
  `title` VARCHAR(256) NULL DEFAULT NULL,
  `is_http` TINYINT(1) NULL DEFAULT 0,
  `is_https` TINYINT(1) NULL DEFAULT 0,
  `scan` TINYINT(1) NULL DEFAULT 0,
  `create_time` BIGINT(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `idx_domain` (`domain` ASC),
  INDEX `idx_code` (`code` ASC),
  INDEX `idx_scan` (`scan` ASC),
  INDEX `idx_time` (`create_time` ASC)
);

# 方便检索
CREATE TABLE IF NOT EXISTS `target` (
  `id` VARCHAR(34) NOT NULL,
  `domain_id` VARCHAR(34) NOT NULL,
  `domain` VARCHAR(45) NOT NULL,
  `target` VARCHAR(45) NOT NULL,
  `title` VARCHAR(256) NULL DEFAULT NULL,
  `match` VARCHAR(1024) NOT NULL,
  `create_time` BIGINT(20) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uniq_domain_target` (`domain_id` ASC, `target` ASC),
  INDEX `idx_target` (`target` ASC),
  INDEX `idx_time` (`create_time` ASC)
);
