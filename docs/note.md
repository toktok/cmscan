# 全自动域名采集及指纹识别系统


# 1. 设置搜索引擎的搜索关键

src/main/resources/keywords.txt

# 2. 采集指纹

src/main/resouces/setting.json

```
   {
      // 表示抓取的网页内容
      "url": "/",
      // 目标页面内容关键词的正则匹配
      "re": "wp-content/themes",
      "name": "wordpress",
      "md5": ""
    }
```

# 3. 测试指纹是否正常

```
python src/main/cmscan.py --target www.easysb.cn
```

查看输出控制台，也可以查看数据库target都行

# 4. 启动系统

```
python src/main/cmscan.py
```

# 5. 查看结果