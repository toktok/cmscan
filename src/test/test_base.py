#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# 主启动脚本
# QQ: 34538980@qq.com
# Jekkay Hu, 2013.5.5
# ///////////////////////////////////////////////////////////////////
#                            _ooOoo_                               //
#                           o8888888o                              //
#                           88" . "88                              //
#                           (| ^_^ |)                              //
#                           O\  =  /O                              //
#                        ____/`---'\____                           //
#                      .'  \\|     |//  `.                         //
#                     /  \\|||  :  |||//  \                        //
#                    /  _||||| -:- |||||-  \                       //
#                    |   | \\\  -  /// |   |                       //
#                    | \_|  ''\---/''  |   |                       //
#                    \  .-\__  `-`  ___/-. /                       //
#                  ___`. .'  /--.--\  `. . ___                     //
#                ."" '<  `.___\_<|>_/___.'  >'"".                  //
#              | | :  `- \`.;`\ _ /`;.`/ - ` : | |                 //
#              \  \ `-.   \_ __\ /__ _/   .-` /  /                 //
#        ========`-.____`-.___\_____/___.-`____.-'========         //
#                             `=---='                              //
#        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^        //
#             佛祖保佑       永无BUG        运行正常                   //
#     -------------------------------------------------------      //
#               QQ: 34538980@qq.com                                //
#               博客: http://www.easysb.cn                          //
#               Jekkay Hu, 2013.5.5                                //
# ///////////////////////////////////////////////////////////////////
#
from os import path
import sys
import unittest

test_folder = path.dirname(path.abspath(__file__))
main_folder = path.abspath(path.join(test_folder, "../main"))
if test_folder not in sys.path:
    sys.path.insert(0, test_folder)
if main_folder not in sys.path:
    sys.path.insert(0, main_folder)
print(sys.path)
from utility.function import get_url


class BaseTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()

    def setUp(self):
        # 每个测试用例执行之前做操作
        super().setUp()

    def tearDown(self):
        # 每个测试用例执行之后做操作
        super().tearDown()

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()

    def test_a_run(self):
        self.assertEqual(1, 1)

    def get_content(self):
        u = 'http://www.baidu.com/link?url=qqs3p0BHG7v9-qhO9ZgWMZKh5Xxu9nsdK4Voh2Zs1b2iuKJsLhY0Hb0CttXCmara'
        content = get_url(u)
        print(content)


