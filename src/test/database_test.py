#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# 主启动脚本
# QQ: 34538980@qq.com
# Jekkay Hu, 2013.5.5
# ///////////////////////////////////////////////////////////////////
#                            _ooOoo_                               //
#                           o8888888o                              //
#                           88" . "88                              //
#                           (| ^_^ |)                              //
#                           O\  =  /O                              //
#                        ____/`---'\____                           //
#                      .'  \\|     |//  `.                         //
#                     /  \\|||  :  |||//  \                        //
#                    /  _||||| -:- |||||-  \                       //
#                    |   | \\\  -  /// |   |                       //
#                    | \_|  ''\---/''  |   |                       //
#                    \  .-\__  `-`  ___/-. /                       //
#                  ___`. .'  /--.--\  `. . ___                     //
#                ."" '<  `.___\_<|>_/___.'  >'"".                  //
#              | | :  `- \`.;`\ _ /`;.`/ - ` : | |                 //
#              \  \ `-.   \_ __\ /__ _/   .-` /  /                 //
#        ========`-.____`-.___\_____/___.-`____.-'========         //
#                             `=---='                              //
#        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^        //
#             佛祖保佑       永无BUG        运行正常                   //
#     -------------------------------------------------------      //
#               QQ: 34538980@qq.com                                //
#               博客: http://www.easysb.cn                          //
#               Jekkay Hu, 2013.5.5                                //
# ///////////////////////////////////////////////////////////////////
#
import unittest
from test_base import BaseTest
from utility.database import Database


class DBTest(BaseTest):
    def test_insert(self):
        self.db = Database()
        self.db.connect_db()
        SQL_INSERT_DOMAIN = 'INSERT IGNORE INTO `domain`(`id`, `domain`,`is_http`, `is_https`,`scan`,`create_time`) VALUES (%(id)s,%(domain)s,%(is_http)s,%(is_https)s,%(scan)s, %(create_time)s)'
        self.db.execute(SQL_INSERT_DOMAIN, {
            'id': 'a',
            'domain': 'b',
            'is_http': 0,
            'is_https': 1,
            'scan': 100,
            'create_time': 100
        })
        self.db.disconnect_db()


if __name__ == '__main__':
    unittest.main()
