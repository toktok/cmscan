#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# 主启动脚本
# QQ: 34538980@qq.com
# Jekkay Hu, 2013.5.5
# ///////////////////////////////////////////////////////////////////
#                            _ooOoo_                               //
#                           o8888888o                              //
#                           88" . "88                              //
#                           (| ^_^ |)                              //
#                           O\  =  /O                              //
#                        ____/`---'\____                           //
#                      .'  \\|     |//  `.                         //
#                     /  \\|||  :  |||//  \                        //
#                    /  _||||| -:- |||||-  \                       //
#                    |   | \\\  -  /// |   |                       //
#                    | \_|  ''\---/''  |   |                       //
#                    \  .-\__  `-`  ___/-. /                       //
#                  ___`. .'  /--.--\  `. . ___                     //
#                ."" '<  `.___\_<|>_/___.'  >'"".                  //
#              | | :  `- \`.;`\ _ /`;.`/ - ` : | |                 //
#              \  \ `-.   \_ __\ /__ _/   .-` /  /                 //
#        ========`-.____`-.___\_____/___.-`____.-'========         //
#                             `=---='                              //
#        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^        //
#             佛祖保佑       永无BUG        运行正常                   //
#     -------------------------------------------------------      //
#               QQ: 34538980@qq.com                                //
#               博客: http://www.easysb.cn                          //
#               Jekkay Hu, 2013.5.5                                //
# ///////////////////////////////////////////////////////////////////
#
import unittest
from test_base import BaseTest
import time
from scan.result_domain_sync_thread import DomainSyncThread
from utility.domain import Domain
import queue


class ResultDomainSyncTest(BaseTest):
    domain_queue = queue.Queue(2)
    thread = DomainSyncThread(domain_queue)

    def test_scan1(self):
        domain = Domain()
        domain.id = "id"
        domain.domain = "domain"
        domain.code = 10
        domain.title = 'title2'
        domain.is_http = 1
        domain.is_https = 1
        domain.scan = 0
        domain.create_time = time.time() * 1000
        self.domain_queue.put(domain)
        self.thread.start()
        time.sleep(600)


if __name__ == '__main__':
    unittest.main()
