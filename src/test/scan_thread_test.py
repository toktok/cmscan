#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# 主启动脚本
# QQ: 34538980@qq.com
# Jekkay Hu, 2013.5.5
# ///////////////////////////////////////////////////////////////////
#                            _ooOoo_                               //
#                           o8888888o                              //
#                           88" . "88                              //
#                           (| ^_^ |)                              //
#                           O\  =  /O                              //
#                        ____/`---'\____                           //
#                      .'  \\|     |//  `.                         //
#                     /  \\|||  :  |||//  \                        //
#                    /  _||||| -:- |||||-  \                       //
#                    |   | \\\  -  /// |   |                       //
#                    | \_|  ''\---/''  |   |                       //
#                    \  .-\__  `-`  ___/-. /                       //
#                  ___`. .'  /--.--\  `. . ___                     //
#                ."" '<  `.___\_<|>_/___.'  >'"".                  //
#              | | :  `- \`.;`\ _ /`;.`/ - ` : | |                 //
#              \  \ `-.   \_ __\ /__ _/   .-` /  /                 //
#        ========`-.____`-.___\_____/___.-`____.-'========         //
#                             `=---='                              //
#        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^        //
#             佛祖保佑       永无BUG        运行正常                   //
#     -------------------------------------------------------      //
#               QQ: 34538980@qq.com                                //
#               博客: http://www.easysb.cn                          //
#               Jekkay Hu, 2013.5.5                                //
# ///////////////////////////////////////////////////////////////////
#
import unittest
from test_base import BaseTest
import queue
import time
from scan.scan_work_thread import ScanWorkThread


class SearchThreadTest(BaseTest):
    thread = ScanWorkThread(queue.Queue(100), queue.Queue(100), queue.Queue(100))

    def test_scan1(self):
        self.thread.scan({
            'id': 100,
            'domain': 'sdpengsuan.com',
            'code': 0,
            'title': None,
            'is_http': 1,
            'is_https': 0,
            'scan': 1,
            'create_time': time.time() * 100
        })
        # thread.start()
        # time.sleep(600)

    def test_scan2(self):
        self.thread.scan({
            'id': 200,
            'domain': 'dedecms.com',
            'code': 0,
            'title': None,
            'is_http': 1,
            'is_https': 0,
            'scan': 1,
            'create_time': time.time() * 100
        })


if __name__ == '__main__':
    unittest.main()
