# ///////////////////////////////////////////////////////////////////
#                            _ooOoo_                               //
#                           o8888888o                              //
#                           88" . "88                              //
#                           (| ^_^ |)                              //
#                           O\  =  /O                              //
#                        ____/`---'\____                           //
#                      .'  \\|     |//  `.                         //
#                     /  \\|||  :  |||//  \                        //
#                    /  _||||| -:- |||||-  \                       //
#                    |   | \\\  -  /// |   |                       //
#                    | \_|  ''\---/''  |   |                       //
#                    \  .-\__  `-`  ___/-. /                       //
#                  ___`. .'  /--.--\  `. . ___                     //
#                ."" '<  `.___\_<|>_/___.'  >'"".                  //
#              | | :  `- \`.;`\ _ /`;.`/ - ` : | |                 //
#              \  \ `-.   \_ __\ /__ _/   .-` /  /                 //
#        ========`-.____`-.___\_____/___.-`____.-'========         //
#                             `=---='                              //
#        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^        //
#             佛祖保佑       永无BUG        运行正常                //
#     -------------------------------------------------------      //
#               QQ: 34538980@qq.com                                //
#               博客: http://www.easysb.cn                         //
#               Jekkay Hu, 2013.5.5                                //
# ///////////////////////////////////////////////////////////////////
from os import path, getcwd

here = path.dirname(path.abspath(__file__))
conf_folder = here if here else path.abspath(getcwd())
resources_folder = path.abspath(path.join(conf_folder, '../resources'))


def load_banner_text():
    with open('%s/banner.txt' % resources_folder, mode='r', encoding='utf-8') as fd:
        while True:
            lines = fd.readlines()
            if not lines:
                break
            return [t.strip(" \r\n") for t in lines if t.strip(" \r\n")]


def print_banner():
    lines = load_banner_text()
    if not lines:
        return
    for line in lines:
        print(line)
