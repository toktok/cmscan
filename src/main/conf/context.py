#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# QQ: 34538980@qq.com
# Jekkay Hu, 2013.5.5
# ///////////////////////////////////////////////////////////////////
#                            _ooOoo_                               //
#                           o8888888o                              //
#                           88" . "88                              //
#                           (| ^_^ |)                              //
#                           O\  =  /O                              //
#                        ____/`---'\____                           //
#                      .'  \\|     |//  `.                         //
#                     /  \\|||  :  |||//  \                        //
#                    /  _||||| -:- |||||-  \                       //
#                    |   | \\\  -  /// |   |                       //
#                    | \_|  ''\---/''  |   |                       //
#                    \  .-\__  `-`  ___/-. /                       //
#                  ___`. .'  /--.--\  `. . ___                     //
#                ."" '<  `.___\_<|>_/___.'  >'"".                  //
#              | | :  `- \`.;`\ _ /`;.`/ - ` : | |                 //
#              \  \ `-.   \_ __\ /__ _/   .-` /  /                 //
#        ========`-.____`-.___\_____/___.-`____.-'========         //
#                             `=---='                              //
#        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^        //
#             佛祖保佑       永无BUG        运行正常                //
#     -------------------------------------------------------      //
#               QQ: 34538980@qq.com                                //
#               博客: http://www.easysb.cn                         //
#               Jekkay Hu, 2013.5.5                                //
# ///////////////////////////////////////////////////////////////////
#
# 当前运行的环境信息
#

from os import path, getcwd
import sys
import logging
from utility.function import load_json_file
from conf.print_banner import load_banner_text
import socket


class Context(object):
    def __init__(self):
        here = path.dirname(path.abspath(__file__))
        self.conf_folder = here if here else path.abspath(getcwd())
        self.resources_folder = path.abspath(path.join(self.conf_folder, '../resources'))
        self.main_folder = path.abspath(path.join(self.conf_folder, '..'))
        self.setting = {}

    def init_path_env(self):
        # 添加main目录
        if self.main_folder not in sys.path:
            sys.path.insert(0, self.main_folder)
        # 打印一下当前的目录
        # print('----- sys path info list ------ ')
        # print('\n'.join(sys.path))
        # print('\n'.join(sys.path))
        # 设置一下日志
        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s %(filename)s:%(lineno)d: %(levelname)s %(message)s',
                            datefmt='%Y-%m-%d %H:%M:%S')
        # console = logging.StreamHandler()
        # console.setLevel(logging.INFO)
        # logging.getLogger().addHandler(console)
        # 读取指纹识别库

        self.setting = load_json_file('%s/setting.json' % self.resources_folder)
        logging.info('setting rules count %s' %
                     (len(self.setting['rules']) if self.setting['rules'] else 0))

        # 设置超时时间
        if self.setting['socket_timeout'] and int(self.setting['socket_timeout']) > 0:
            socket.setdefaulttimeout(int(self.setting['socket_timeout']))
            logging.info('set default socket time [ %s ] seconds!' % self.setting['socket_timeout'])

        # 打印banner
        read_lines = load_banner_text()
        for line in read_lines:
            logging.info(line)


# 全局的变量
context = Context()
context.init_path_env()

if __name__ == '__main__':
    print(context.conf_folder)
