#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# QQ: 34538980@qq.com
# Jekkay Hu, 2013.5.5
# ///////////////////////////////////////////////////////////////////
#                            _ooOoo_                               //
#                           o8888888o                              //
#                           88" . "88                              //
#                           (| ^_^ |)                              //
#                           O\  =  /O                              //
#                        ____/`---'\____                           //
#                      .'  \\|     |//  `.                         //
#                     /  \\|||  :  |||//  \                        //
#                    /  _||||| -:- |||||-  \                       //
#                    |   | \\\  -  /// |   |                       //
#                    | \_|  ''\---/''  |   |                       //
#                    \  .-\__  `-`  ___/-. /                       //
#                  ___`. .'  /--.--\  `. . ___                     //
#                ."" '<  `.___\_<|>_/___.'  >'"".                  //
#              | | :  `- \`.;`\ _ /`;.`/ - ` : | |                 //
#              \  \ `-.   \_ __\ /__ _/   .-` /  /                 //
#        ========`-.____`-.___\_____/___.-`____.-'========         //
#                             `=---='                              //
#        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^        //
#             佛祖保佑       永无BUG        运行正常                   //
#     -------------------------------------------------------      //
#               QQ: 34538980@qq.com                                //
#               博客: http://www.easysb.cn                          //
#               Jekkay Hu, 2013.5.5                                //
# ///////////////////////////////////////////////////////////////////
#
# 从搜索引擎搜索指定的关键词，采集初始域名
#
import threading
import logging
import time
from utility.database import Database


class BaseDBOpThread(threading.Thread):
    def __init__(self):
        super(BaseDBOpThread, self).__init__()
        # 数据库
        self.db = None
        # 是否正则工作中
        self.is_working = False
        # 设置为非Daemon
        self.setDaemon(False)
        # 退出的标志位
        self.exit_flag = False

    def _create_db_connection(self):
        if not self.db:
            self.db = Database()
        if (not self.db.is_connected()) and (not self.db.connect_db()):
            self.db.disconnect_db()
        return self.db.is_connected()

    def _close_db_connect(self):
        if self.db and self.db.is_connected():
            self.db.disconnect_db()

    def auto_check_db(self):
        if (not self.db) or (not self.db.is_connected()):
            self._create_db_connection()
        if (not self.db) or (not self.db.is_connected()):
            logging.error("fail to connect to db")
            return False
        return True

    def run(self):
        while not self.exit_flag:
            try:
                self.is_working = False
                if not self.auto_check_db():
                    time.sleep(10)
                    continue
                self.is_working = True
                self.work_loop()
            except Exception as e:
                logging.error('BaseDBOpThread %s' % str(e))
                self._close_db_connect()
                time.sleep(2)

    def work_loop(self):
        pass

