#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# QQ: 34538980@qq.com
# Jekkay Hu, 2013.5.5
# ///////////////////////////////////////////////////////////////////
#                            _ooOoo_                               //
#                           o8888888o                              //
#                           88" . "88                              //
#                           (| ^_^ |)                              //
#                           O\  =  /O                              //
#                        ____/`---'\____                           //
#                      .'  \\|     |//  `.                         //
#                     /  \\|||  :  |||//  \                        //
#                    /  _||||| -:- |||||-  \                       //
#                    |   | \\\  -  /// |   |                       //
#                    | \_|  ''\---/''  |   |                       //
#                    \  .-\__  `-`  ___/-. /                       //
#                  ___`. .'  /--.--\  `. . ___                     //
#                ."" '<  `.___\_<|>_/___.'  >'"".                  //
#              | | :  `- \`.;`\ _ /`;.`/ - ` : | |                 //
#              \  \ `-.   \_ __\ /__ _/   .-` /  /                 //
#        ========`-.____`-.___\_____/___.-`____.-'========         //
#                             `=---='                              //
#        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^        //
#             佛祖保佑       永无BUG        运行正常                   //
#     -------------------------------------------------------      //
#               QQ: 34538980@qq.com                                //
#               博客: http://www.easysb.cn                          //
#               Jekkay Hu, 2013.5.5                                //
# ///////////////////////////////////////////////////////////////////
#
# 对html内容解码

# -*- coding: utf-8 -*-
# @Time    : 2018/5/4 0004 8:55
# @Author  : Langzi
# @Blog    : www.langzi.fun
# @File    : get urls.py
# @Software: PyCharm
import chardet


class Charset(object):
    _INNER_ENCODING_LIST = ['UTF-8', 'GBK', 'GB2312']
    @staticmethod
    def decode(html):
        if not html:
            return html
        res = Charset._decode_encoding(html, None)
        if res:
            return res
        # 自动检测下当前的编码
        codesty = chardet.detect(html)
        if codesty and codesty['encoding']:
            res = Charset._decode_encoding(html, codesty['encoding'])
            if res:
                return res
        for encoding in Charset._INNER_ENCODING_LIST:
            res = Charset._decode_encoding(html, encoding)
            if res:
                return res
        return html

    @staticmethod
    def _decode_encoding(html, encoding):
        try:
            return html.decode(encoding) if encoding else html.decode()
        except Exception as e:
            pass

