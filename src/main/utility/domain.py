#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# QQ: 34538980@qq.com
# Jekkay Hu, 2013.5.5
# ///////////////////////////////////////////////////////////////////
#                            _ooOoo_                               //
#                           o8888888o                              //
#                           88" . "88                              //
#                           (| ^_^ |)                              //
#                           O\  =  /O                              //
#                        ____/`---'\____                           //
#                      .'  \\|     |//  `.                         //
#                     /  \\|||  :  |||//  \                        //
#                    /  _||||| -:- |||||-  \                       //
#                    |   | \\\  -  /// |   |                       //
#                    | \_|  ''\---/''  |   |                       //
#                    \  .-\__  `-`  ___/-. /                       //
#                  ___`. .'  /--.--\  `. . ___                     //
#                ."" '<  `.___\_<|>_/___.'  >'"".                  //
#              | | :  `- \`.;`\ _ /`;.`/ - ` : | |                 //
#              \  \ `-.   \_ __\ /__ _/   .-` /  /                 //
#        ========`-.____`-.___\_____/___.-`____.-'========         //
#                             `=---='                              //
#        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^        //
#             佛祖保佑       永无BUG        运行正常                   //
#     -------------------------------------------------------      //
#               QQ: 34538980@qq.com                                //
#               博客: http://www.easysb.cn                          //
#               Jekkay Hu, 2013.5.5                                //
# ///////////////////////////////////////////////////////////////////
from conf.const import SWITCH_INIT, SCAN_STATE_INIT
from utility.function import extract_schema, extract_host, random_md5, is_legal_domain, get_root_domain
import time


class Domain(object):
    def __init__(self):
        self.id = None
        self.domain = None
        self.code = 0
        self.title = None
        self.is_http = SWITCH_INIT
        self.is_https = SWITCH_INIT
        self.scan = SCAN_STATE_INIT
        # 毫秒
        self.create_time = time.time() * 1000

    @staticmethod
    def convert(url):
        if not url and url.find("://") < 0:
            return
        host = extract_host(url)
        scheme = extract_schema(url)
        if not host or not scheme or not is_legal_domain(host):
            return
        # 只取根域名
        root_domain = get_root_domain(host)
        if not is_legal_domain(root_domain):
            return
        d = Domain()
        d.id = random_md5()
        d.domain = root_domain
        d.is_http = 1 if scheme.lower() == 'http' else 0
        d.is_https = 1 if scheme.lower() == 'https' else 0
        return d

