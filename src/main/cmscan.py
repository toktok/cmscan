#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# 主启动脚本
# QQ: 34538980@qq.com
# Jekkay Hu, 2013.5.5
# ///////////////////////////////////////////////////////////////////
#                            _ooOoo_                               //
#                           o8888888o                              //
#                           88" . "88                              //
#                           (| ^_^ |)                              //
#                           O\  =  /O                              //
#                        ____/`---'\____                           //
#                      .'  \\|     |//  `.                         //
#                     /  \\|||  :  |||//  \                        //
#                    /  _||||| -:- |||||-  \                       //
#                    |   | \\\  -  /// |   |                       //
#                    | \_|  ''\---/''  |   |                       //
#                    \  .-\__  `-`  ___/-. /                       //
#                  ___`. .'  /--.--\  `. . ___                     //
#                ."" '<  `.___\_<|>_/___.'  >'"".                  //
#              | | :  `- \`.;`\ _ /`;.`/ - ` : | |                 //
#              \  \ `-.   \_ __\ /__ _/   .-` /  /                 //
#        ========`-.____`-.___\_____/___.-`____.-'========         //
#                             `=---='                              //
#        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^        //
#             佛祖保佑       永无BUG        运行正常                   //
#     -------------------------------------------------------      //
#               QQ: 34538980@qq.com                                //
#               博客: http://www.easysb.cn                          //
#               Jekkay Hu, 2013.5.5                                //
# ///////////////////////////////////////////////////////////////////
#
import argparse
from os import path, getcwd, system
import sys

here = path.dirname(path.abspath(__file__))
main_folder = here if here else path.abspath(getcwd())
if main_folder not in sys.path:
    sys.path.insert(0, main_folder)

parser = argparse.ArgumentParser(description='全自动化的域名采集及指纹识别系统',
                                 epilog='有任何疑问请联系: 34538980@qq.com')
parser.add_argument('-t', '--target', nargs='*', type=str, help='扫描指纹的目标域名')
parser.add_argument('-s', '--start', action='store_true', help='启动全自动化的域名采集及指纹识别系统')
parser.add_argument('-k', '--kill', '--stop', action='store_true', help='停止全自动化的域名采集及指纹识别系统')
parser.add_argument('-d', '--daemon', action='store_true', help='开启守护进程模式(Linux下有效)')


def command_scan_target(*target):
    print('scan %s' % str(target))
    from scan.main_scan import MainScan
    MainScan().scan_targets(*target)


def command_kill():
    print('command_kill')
    system('ps aux | grep cmscan | grep -v grep | awk \'{print $2}\' | xargs kill -9')


def command_start():
    print('command_start')
    from scan.main_scan import MainScan
    MainScan().start_system()


def command_daemon_start():
    print('command_daemon_start')
    import daemon
    from scan.main_scan import MainScan
    with daemon.DaemonContext():
        MainScan().start_system()


if __name__ == '__main__':
    args = vars(parser.parse_args())
    print(args)
    if args['target']:
        command_scan_target(*args['target'])
    elif args['kill']:
        command_kill()
    elif args['start']:
        if args['daemon']:
            command_daemon_start()
        else:
            command_start()
    else:
        command_start()
